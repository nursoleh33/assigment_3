/* This is the main.js file */
let questions = {
    0: {
        soal: "Bibi datang mengendarai mobil. Bibi membunyikan klakson. Bunyinya ....",
        jawaban: {
            0: {
                text: "tok tok",
                score: 0
            },
            1: {
                text: "ting ting",
                score: 0
            },
            2: {
                text: "din din",
                score: 1
            }
        }
    },
    1: {
        soal: "Kakak menangis karena balonnya meletus. Balon meletus bunyinya",
        jawaban: {
            0: {
                text: "prang",
                score: 0
            },
            1: {
                text: "dor",
                score: 1
            },
            2: {
                text: "dung dung",
                score: 0
            }
        }
    },
    2: {
        soal: "Ketika pulang sekolah Siti bertemu bu guru. Siti Mengucapkan ....",
        jawaban: {
            0: {
                text: "halo bu guru",
                score: 0
            },
            1: {
                text: "hai bu guru",
                score: 0
            },
            2: {
                text: "selamat siang bu",
                score: 1
            }
        }
    },
    3: {
        soal: "Kakak sedang menjemur pakaian. Pakaian dijemur agar ....",
        jawaban: {
            0: {
                text: "bagus",
                score: 0
            },
            1: {
                text: "kering",
                score: 1
            },
            2: {
                text: "wangi",
                score: 0
            }
        }
    },
    4: {
        soal: "Adik mengambil sapu ijuk. Adik akan menyapu ....",
        jawaban: {
            0: {
                text: "lantai",
                score: 1
            },
            1: {
                text: "halaman",
                score: 0
            },
            2: {
                text: "ruangan",
                score: 0
            }
        }
    },
    5: {
        soal: "Berikut ini buah yang bentuknya bulat adalah ....",
        jawaban: {
            0: {
                text: "salak",
                score: 0
            },
            1: {
                text: "pepaya",
                score: 0
            },
            2: {
                text: "jeruk",
                score: 1
            }
        }
    },
    6: {
        soal: "Ayah Dono sedang mengemudikan kereta api. Ayah Dono seorang ",
        jawaban: {
            0: {
                text: "sopir",
                score: 0
            },
            1: {
                text: "pilot",
                score: 0
            },
            2: {
                text: "masinis",
                score: 1
            }
        }
    },
    7: {
        soal: "Kakak naik kendaraan roda tiga. Kendaraan ini bisa dikayuh. Kendaraan ini namanya ....",
        jawaban: {
            0: {
                text: "mobil",
                score: 0
            },
            1: {
                text: "delman",
                score: 0
            },
            2: {
                text: "becak",
                score: 1
            }
        }
    },
    8: {
        soal: "Merry membersihkan meja menggunakan ....",
        jawaban: {
            0: {
                text: "kain pel",
                score: 0
            },
            1: {
                text: "kemoceng",
                score: 1
            },
            2: {
                text: "sapu",
                score: 0
            }
        }
    },
    9: {
        soal: "Althaf haus. Althaf mengambil .... untuk minum.",
        jawaban: {
            0: {
                text: "mangkuk",
                score: 0
            },
            1: {
                text: "piring",
                score: 0
            },
            2: {
                text: "gelas",
                score: 1
            }
        }
    },
    
}
// console.log(questions[0].soal); //mengkases soal
// console.log(questions[0].jawaban) //mengakses Jawaban
let questionText = "";
let next = "";
let prev = "";
let finish = "";
let containerQuiz = document.querySelector('.thumbnail');
let isHide = "";
    for(let key in questions){
        if(key != 0 ){
            prev = `
            <button type="button" onclick="control('question-${parseInt(key)}', 'question-${parseInt(key)+1}')">Prev</button>
            `
            isHide = "hide";
        }
        if(key == 9){
            next = ''; //jika sudah mendapatkan score 9 maka tidak bisa melakukan action next
            finish = `
            <button type="button" onclick="showResult()">Finish</button>
                    
            `
        }else{
            next = `<button type="button" onclick="control('question-${parseInt(key)+2}', 'question-${parseInt(key)+1}')">Next</button>`
            finish = ""; //jika question masih ada dan scrope belum memenuhi, maka button finsih hilang
        }

    questionText += `   <div class="box ${isHide}" id="question-${parseInt(key)+1}">
                            <div class="box__heading"> 
                                <h1>Pertayaan ${parseInt(key)+1}</h1>
                                <p>${questions[key].soal}</p>
                                <div class="box__pilihan">
                                <label>
                                    <input type="radio" name="answer-${key}" value="${questions[key].jawaban[0].score}">
                                ${questions[key].jawaban[0].text}
                                </label>
                                <label>
                                    <input type="radio" name="answer-${key}" value="${questions[key].jawaban[1].score}">
                                ${questions[key].jawaban[1].text}
                                </label>
                                <label>
                                    <input type="radio" name="answer-${key}" value="${questions[key].jawaban[2].score}">
                                ${questions[key].jawaban[2].text}
                                </label>
                                </div>
                                <div class="box__action">
                                    ${prev}
                                    ${next}
                                    ${finish}
                                </div>
                            </div>
                        </div>   
                                `
}
containerQuiz.innerHTML = questionText;

function control(next, prev){
    document.getElementById(next).classList.remove('hide');
    document.getElementById(prev).classList.add('hide');
    
    
}
let result = document.querySelector('.winner');
let finalResult = '';
function showResult(){
    let total = 0; //nilai scroll awal 0
    let message = '';
    let color = '';
    for(let k =0; k<10; k++){
        //input name answer sesuai dengan input dengan type="radio".
        total += parseInt(document.querySelector(`input[name=answer-${k}]:checked`).value)
    }
    if(total < 7){
        message = 'Yah...belum lulus nih. Belajar lagi ya dek!'
        color = 'red'
    }else if(total >= 7 && total < 10){
        message = "Nilai kamu bagus!!! pertahankan Ya"
        color = 'blue'
    }else if(total == 10){
        message = "Sempurna! Keren banget kamu!"
        color = 'green'
    }
    
    
    let finalResult = `
                    <h6>Hi, ${localStorage.getItem("username")}! Nilai Kamu :</h6>
                    <h2 class="${color}">${total} / 10</h2>
                    <p>${message}</p>
                    `
                    // username mengambil dari local storage dengan key username regis.
    document.querySelector('.thumbnail').classList.add('hide');
    result.innerHTML = finalResult;
    result.classList.remove('hide');
    result.classList.add('show');
}


// Color Mode
var containQuestion = document.querySelectorAll('.box');
var containQuizLagi = document.querySelector('.container');
// console.log(containQuestion);
function changeMode(){
    let result = document.querySelector('.winner');
    for(let g =0; g <containQuestion.length; g++){
        let mode = localStorage.getItem('mode');
        if(localStorage.getItem('mode') == 'dark'){
            containQuizLagi.style.backgroundColor="#555"
            containQuizLagi.style.color="#eaeaea"
            containQuestion[g].style.backgroundColor="#555"
            containQuestion[g].style.color="#eaeaea"
            result.style.backgroundColor="#555"
            result.style.color="#eaeaea"
        }else if(localStorage.getItem('mode') == 'light'){
            containQuizLagi.style.backgroundColor="#eaeaea"
            containQuizLagi.style.color="#555"
            containQuestion[g].style.backgroundColor="#eaeaea"
            containQuestion[g].style.color="#555"  
            result.style.backgroundColor="#eaeaea"
            result.style.color="#555"
        }
        document.getElementById('mode').value = mode
    }
    // return
}
changeMode()
document.getElementById('mode').addEventListener('change', function(){
    if(this.value == 'light'){
        localStorage.setItem('mode', 'light');
    }else{
        localStorage.setItem('mode', 'dark');
    }
    changeMode()
})

var component = {
    regis : () =>{
        return `
                <h2>Register</h2>
                <div class="container__form">
                    <label for="username">Username</label>
                    <input type="email" id="username" class="input" placeholder="Username" autofocus="on" autocomplete="on">
                    <span class="container__pesanError"></span>
                </div>
                <div class="container__form">
                    <label for="password">Password</label>
                    <input type="password" id="password" class="pass" placeholder="Password" autofocus="on" autocomplete="on">
                    <span class="container__pesanError"></span>
                        <button type="button" id="tampilPass" class="tampilPass" onclick="tampil()">Show Password</button>
                </div>
                <div class="container__btnDaftar">
                    <p>Already have an account?? </p>
                    <a id="menuDaftar">Login</a>
                    <div class="container__btnSingup">
                        <button type="button" id="tombol">Sign Up</button>
                    </div>
                </div>
                `;
    },
    log : () => {
        return `
                <h2>Form Login</h2>
                <div class="container__form">
                <label for="username">Username</label>
                <input type="email" id="username" class="input" placeholder="Masukan Username Anda" autofocus="on" autocomplete="on">
                <span class="container__pesanError"></span>
                </div>
                <div class="container__form">
                    <label for="password">Password</label>
                    <input type="password" id="password" class="pass" placeholder="Masukan Password Anda" autofocus="on" autocomplete="on">
                    <span class="container__pesanError"></span>
                        <button type="button" id="tampilPass" class="tampilPass" onclick="tampil()">Show Password</button>
                </div>
                <div class="container__btnDaftar">
                    <p>Have Account ?? <a id="menuLogin">Daftar</a></p>
                    
                    <div class="container__btnSingup">
                        <button type="button" id="tombolLogin" class="login">Singin</button>
                    </div>
                </div>
                `;
    },
    quiz : () => {
        return `
                <h2>Welcome to Quizz Game</h2>
                <div class="container__form">
                <input type="email" class="gameQuiz" id="username" placeholder="Masukan Username Anda" >
                <span class="container__pesanError"></span>
                </div>
                <div class="container__btnStart">
                    <button type="button" value="simpan" id="btnQuiz" onclick="question()">Yuk Mulai</button>
                </div>
                `
    }
};
var dataFrom = document.querySelector('form');
// console.log(dataFrom);
dataFrom.innerHTML = component.regis();
// console.log(component.regis());
var accountUsr={};
var accountPas={};
accountUsr.username=[];
accountPas.password=[];
var btnRegis = document.getElementById('tombol');
btnRegis.addEventListener('click', function(){
    var usernameRegis = document.getElementById('username').value;
    var userpassRegis = document.getElementById('password').value;
    var user = document.getElementById('username');
    var pass = document.getElementById('password');
    let inValid = true;
    if(user.value == ""){
        user.style.border ="2px solid red";
        user.nextElementSibling.innerHTML = 'wajib di isi'
        inValid = false
    }
    if(pass.value == ""){
        pass.style.border ="2px solid red";
        pass.nextElementSibling.innerHTML = 'wajib di isi'
        inValid = false
    }
    if(inValid == false){
        alert('From Validation Gagal !')
    }
    user.addEventListener('keyup', function(){
        this.style.border="none";
        this.nextElementSibling.innerHTML="";
    })
    pass.addEventListener('keyup', function(){
        this.style.border="none";
        this.nextElementSibling.innerHTML="";
    })
    
    accountUsr.username.push(usernameRegis);
    accountPas.password.push(userpassRegis);
    
    

    sessionStorage.setItem("accountUsr",JSON.stringify(accountUsr));
    sessionStorage.setItem("accountPas",JSON.stringify(accountPas));
    dataFrom.reset()
    console.log(sessionStorage.getItem('accountUsr'));
    console.log(sessionStorage.getItem('accountPas'));
})
function quiz(){
    dataFrom.innerHTML = component.quiz();
}
function question(){
    let usernameRegis = document.getElementById('username').value;
    var user = document.getElementById('username');
    let inValid = true;
    if(user.value == ""){
        user.style.border ="2px solid red";
        user.nextElementSibling.innerHTML = 'wajib di isi'
        inValid = false
    }
    if(inValid == false){
        alert('From Validation Gagal !')

    }
    user.addEventListener('keyup', function(){
        this.style.border="none";
        this.nextElementSibling.innerHTML="";
    })
    let getAccountUsr = sessionStorage.getItem('accountUsr');

    let resultAccount = JSON.parse(getAccountUsr);
    var dataAccountUsr = resultAccount.username;
    // console.log(dataAccountUsr);
    // console.log(resultAccount.username[0]);

    for(let b = 0; b < dataAccountUsr.length; b++){
        if(resultAccount.username[b]==usernameRegis){
            alert(`Selamat Berhasil Masuk Game Quiz ${usernameRegis}`)
            //mengambil username dari alert(username sesuai dengan hasil regis & login ) lalu kita simpan kedalam localstorage dengan key username dan value dari usernameregis
            localStorage.setItem('username', usernameRegis);
            containQuizLagi.classList.add('hide');
            document.querySelector('.thumbnail').classList.remove('hide');
            document.querySelector('.thumbnail').classList.add('show');
        }else if(resultAccount.username[b] !== usernameRegis){
            return alert('Username yang anda masukan tidak sesuai');
        }else{
            alert('Tidak sesuai')
        }
    }
}
var dataLogin = document.querySelector('#menuDaftar');
// console.log(dataLogin)
dataLogin.addEventListener('click', function(){
    dataFrom.innerHTML = component.log();

    var menuLog = document.querySelector('#menuLogin');
    menuLog.addEventListener('click', function(){
        dataFrom.innerHTML = component.regis();
    });

    var btnLogin = document.querySelector('#tombolLogin');
    btnLogin.addEventListener('click', function(){
        let usernameRegis = document.getElementById('username').value;
        let userpassRegis = document.getElementById('password').value;

        var user = document.getElementById('username');
        var pass = document.getElementById('password');
        let inValid = true;
        if(user.value == ""){
            user.style.border ="2px solid red";
            user.nextElementSibling.innerHTML = 'wajib di isi'
            inValid = false
        }
        if(pass.value == ""){
            pass.style.border ="2px solid red";
            pass.nextElementSibling.innerHTML = 'wajib di isi'
            inValid = false
        }
        if(inValid == false){
            alert('From Validation Gagal !')
        }
        user.addEventListener('keyup', function(){
            this.style.border="none";
            this.nextElementSibling.innerHTML="";
        })
        pass.addEventListener('keyup', function(){
            this.style.border="none";
            this.nextElementSibling.innerHTML="";
        })

        let rawAccountPas=sessionStorage.getItem("accountPas");
        let rawAccountUsr=sessionStorage.getItem("accountUsr");
        var savedAccountUsr=JSON.parse(rawAccountUsr);
        var savedAccountPas=JSON.parse(rawAccountPas);
        var dataUsr = savedAccountUsr.username;
        var dataPass = savedAccountPas.password;
        // console.log(savedAccountUsr.username[1]);
        // console.log(savedAccountPas.password[1]);
        for(let i =0; i < dataUsr.length;i++){
            if(savedAccountUsr.username[i]==usernameRegis){
                for(var j =0; j < dataPass.length;j++){
                    if(savedAccountPas.password[j]==userpassRegis){
                        alert(`Anda Berhasil Login ${usernameRegis}`)
                        return quiz();
                    }else if(savedAccountPas.password[j] !== userpassRegis){
                        return alert("Password & Username Tidak sama");
                    }
                }
            }else if(savedAccountUsr.username[i] !== usernameRegis){
                return alert('Akun belum terdaftar, silahkan registrasi terlebih dahulu');
            }
        }
        
    });
});

function tampil(){
    var passwordData = document.querySelector('.pass');
    var btnTampil = document.querySelector('#tampilPass');
    if(passwordData.type == "password"){
        passwordData.type ="text"; //merubah type password menjadi text
        btnTampil.innerHTML = 'Hide Password';
    }else{
        passwordData.type = "password" ;//merubah type menjadi password/tersembunyi
        btnTampil.innerHTML = 'Show Password';
    }
}


document.body.addEventListener('mousemove', function(event){
    var xPos = Math.round((event.clientX / window.innerWidth) * 255);
    var yPos = Math.round((event.clientY / window.innerHeight) * 255);
    document.body.style.backgroundColor = 'rgb('+ xPos +','+ yPos +',100)';
})